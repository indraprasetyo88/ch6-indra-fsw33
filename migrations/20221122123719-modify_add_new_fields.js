'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn("user_game_biodata", "phone", {
        type: Sequelize.STRING,
        allowNull: true,
      }),
    
      queryInterface.addColumn("user_game_biodata", "birthdate", {
        type: Sequelize.STRING,
        allowNull: true,
      }),
      queryInterface.addColumn("user_game_biodata", "city", {
        type: Sequelize.STRING,
        allowNull: true,
      }),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};

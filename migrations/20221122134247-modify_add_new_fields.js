'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    
  },

  async down (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.removeColumn("user_game_biodata", "user_id")
    ]);
  }
};

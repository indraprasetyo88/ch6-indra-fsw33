const express = require("express");
const app = express();
const fs = require("fs");
const expressSession = require("express-session");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static("./public"));

app.set("view engine", "ejs");

app.get("/", (req, res) => {
  const { username } = req.params;
  res.render("index.ejs", { username });
});

app.get("/login", (req, res) => {
  res.render("login.ejs");
});

app.get("/users", (req, res) => {
  res.send(data);
});

app.use((err, req, res, next) => {
  res.status(404).render("404.ejs");
});

app.use(
  expressSession({
    secret: "ewfewf",
    cookie: {
      maxAge: 1000 * 60 * 60, // 1 hour
    },
  })
);

require("./controllers")(app);

app.listen(4000, () => console.log(`Listening at http://localhost:${4000}`));
